package com.pmp.stock.properties;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/*
 * Used for @StockWatchDog
 */
@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "watchdog")
public class WatchDogConfiguration {
	@NotBlank
	private String apiKey;
	@NotBlank
	private String equityQuoteURI;
	@NotBlank
	private long stockCheckIntervalInMiliseconds;

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getEquityQuoteURI() {
		return equityQuoteURI;
	}

	public void setEquityQuoteURI(String equityQuoteURI) {
		this.equityQuoteURI = equityQuoteURI;
	}

	public long getStockCheckIntervalInMiliseconds() {
		return stockCheckIntervalInMiliseconds;
	}

	public void setStockCheckIntervalInMiliseconds(long stockCheckIntervalInMiliseconds) {
		this.stockCheckIntervalInMiliseconds = stockCheckIntervalInMiliseconds;
	}

}
