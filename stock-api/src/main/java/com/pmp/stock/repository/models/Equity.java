package com.pmp.stock.repository.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * A type of stock.
 */
@Entity
@Table(name = "equity")
public class Equity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(max = 50)
	@Column(length = 50, unique = true)
	private String symbol;

	@Size(max = 50)
	@Column(length = 50)
	private String name;

	@Size(max = 50)
	@Column(length = 50)
	private String type;

	@Size(max = 50)
	@Column(length = 50)
	private String region;

	@Size(max = 20)
	@Column(name = "market_open")
	private String marketOpen;

	@Size(max = 20)
	@Column(name = "market_close")
	private String marketClose;

	@Size(max = 20)
	private String timezone;

	@Size(max = 20)
	private String currency;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getMarketOpen() {
		return marketOpen;
	}

	public void setMarketOpen(String marketOpen) {
		this.marketOpen = marketOpen;
	}

	public String getMarketClose() {
		return marketClose;
	}

	public void setMarketClose(String marketClose) {
		this.marketClose = marketClose;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Equity equity = (Equity) o;
		return !(equity.getSymbol() == null || getSymbol() == null) && Objects.equals(getSymbol(), equity.getSymbol());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Equity{" + "symbol='" + symbol + '\'' + ", name='" + name + '\'' + ", type='" + type + '\''
				+ ", region='" + region + '\'' + ", marketOpen='" + marketOpen + '\'' + ", marketClose='" + marketClose
				+ '\'' + ", timezone='" + timezone + '\'' + ", currency='" + currency + '\'' + "}";
	}
}
