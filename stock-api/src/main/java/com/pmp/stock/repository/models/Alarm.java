package com.pmp.stock.repository.models;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Alarm set by user.
 */
@Entity
@Table(name = "alarm")
public class Alarm {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(nullable = false)
	private Double min;

	@NotNull
	@Column(nullable = false)
	private Double max;

	@NotNull
	@Column(nullable = false)
	private Double value;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "USER_ID")
	private User user;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "EQUITY_ID")
	private Equity equity;
	
	private boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Equity getEquity() {
		return equity;
	}

	public void setEquity(Equity equity) {
		this.equity = equity;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Alarm alarm = (Alarm) o;
		return !(alarm.getId() == null || getId() == null) && Objects.equals(getId(), alarm.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

}
