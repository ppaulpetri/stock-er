package com.pmp.stock.repository.init;
import javax.transaction.Transactional;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.pmp.stock.properties.AuthoritiesConstants;
import com.pmp.stock.repository.AuthorityRepository;
import com.pmp.stock.repository.models.Authority;

/**
 * Adding default data to the DB, like user authorities
 */
@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

	boolean alreadySetup = false;

	private final AuthorityRepository authorityRepository;

	public InitialDataLoader(AuthorityRepository authorityRepository) {
		this.authorityRepository = authorityRepository;
	}

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		  if (alreadySetup)
	            return;

		  authorityRepository.findById(AuthoritiesConstants.USER).orElse(createAuthority(AuthoritiesConstants.USER));
	      authorityRepository.findById(AuthoritiesConstants.ADMIN).orElse(createAuthority(AuthoritiesConstants.ADMIN));
	
	      alreadySetup = true;
	}
	
	private Authority createAuthority(String name){
		Authority authority = new Authority();
		authority.setName(name);
		authorityRepository.save(authority);
		
		return authority;
	}
}
