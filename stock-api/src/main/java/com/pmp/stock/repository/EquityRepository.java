package com.pmp.stock.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pmp.stock.repository.models.Equity;

/**
 * Spring Data JPA repository for the @Authority entity.
 */
@Repository
public interface EquityRepository extends JpaRepository<Equity, Long> {
	Optional<Equity> findOneBySymbol(String symbol);
}
