package com.pmp.stock.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.pmp.stock.repository.models.Authority;

/**
 * Spring Data JPA repository for the @Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
