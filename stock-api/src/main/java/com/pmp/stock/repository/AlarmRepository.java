package com.pmp.stock.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.pmp.stock.repository.models.Alarm;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface AlarmRepository extends JpaRepository<Alarm, Long> {

	List<Alarm> findAllByUserId(Long id);

	Optional<Alarm> findOneByIdAndUserId(Long id, Long userId);

	List<Alarm> findAllByEquityIdAndActiveIsTrueAndMaxLessThan(long equityId, Double max);

	List<Alarm> findAllByEquityIdAndActiveIsTrueAndMinGreaterThan(long equityId, Double min);
	
}
