package com.pmp.stock.rest.v1;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.pmp.stock.repository.models.Alarm;
import com.pmp.stock.rest.errors.InvalidPasswordException;
import com.pmp.stock.service.AlarmService;
import com.pmp.stock.service.dto.AlarmDTO;

/**
 * Rest Controller for @Alarm.
 */ 
@RestController
@RequestMapping("/api")
public class AlarmController {

	private final AlarmService alarmService;

	public AlarmController(AlarmService alarmService) {
		this.alarmService = alarmService;
	}

	/**
	 * POST /alarm : create alarm.
	 *
	 * @param alarmDTO
	 * 
	 */
	@PostMapping("/alarms")
	@Timed
	@ResponseStatus(HttpStatus.CREATED)
	public Alarm createAlarm(@Valid @RequestBody AlarmDTO alarmDTO) {
		return alarmService.createAlarm(alarmDTO);
	}

	/**
	 * POST /alarm : create alarm.
	 *
	 * @param alarmDTO
	 *
	 */
	@PutMapping("/alarms")
	@Timed
	@ResponseStatus(HttpStatus.OK)
	public AlarmDTO updateAlarm(@Valid @RequestBody AlarmDTO alarmDTO) {
		return alarmService.updateAlarm(alarmDTO);
	}

	/**
	 * Patch /alarm/{id}/status/{isActive} : change alarm status.
	 *
	 */
	@PatchMapping("/alarms/{id}/status/{isActive}")
	@Timed
	@ResponseStatus(HttpStatus.OK)
	public void chageStatus(@PathVariable("id") long id, @PathVariable("isActive") boolean isActive) {
		alarmService.changeActiveStatus(isActive, id);
	}

	/**
	 * Patch /alarm/{id}/status/{isActive} : change alarm status.
	 *
	 */
	@DeleteMapping("/alarms/{id}")
	@Timed
	@ResponseStatus(HttpStatus.OK)
	public void deleteAlarm(@PathVariable("id") long id) {
		alarmService.deleteAlarm(id);
	}

	/**
	 * GET /alarm/all : get all alarms for user.
	 *
	 * @return alarms for user
	 */
	@GetMapping("/alarms")
	@Timed
	public List<AlarmDTO> getAlarmsForUser() {
		return alarmService.getAllAlrmsForUser();
	}

	/**
	 * GET /alarm/all : get all alarms for user.
	 *
	 * @return alarms for user
	 */
	@GetMapping("/alarms/{id}")
	@Timed
	public AlarmDTO getAlarm(@PathVariable("id") long id) {
		return alarmService.getAlarm(id);
	}
}
