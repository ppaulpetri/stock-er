package com.pmp.stock.service.dto;

import com.pmp.stock.repository.models.Alarm;

public class AlarmDTO {

	private Long id;

	private Double min;

	private Double max;

	private Double value;

	private EquityDTO equity;
	
	private boolean active;

	public AlarmDTO() {
		// Empty constructor needed for Jackson.
	}
	
	public AlarmDTO(Alarm alarm) {
        this.id = alarm.getId();
        this.min = alarm.getMin();
        this.max = alarm.getMax();
        this.value = alarm.getValue();
        this.active = alarm.isActive();
        this.equity = new EquityDTO(alarm.getEquity());
    }
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public EquityDTO getEquity() {
		return equity;
	}

	public void setEquity(EquityDTO equity) {
		this.equity = equity;
	}

}
