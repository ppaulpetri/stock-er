package com.pmp.stock.service;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pmp.stock.repository.EquityRepository;
import com.pmp.stock.repository.models.Equity;
import com.pmp.stock.service.dto.EquityDTO;

/**
 * Service class for managing equities.
 */
@Service
@Transactional
public class EquityService {

	private final EquityRepository equityRepository;

	public EquityService(EquityRepository equityRepository) {
		this.equityRepository = equityRepository;
	}

	public Equity GetOrCreateEquity(EquityDTO equityDTO) {
		Optional<Equity> equity = equityRepository.findOneBySymbol(equityDTO.getSymbol());
		return equity.orElseGet(() -> equityRepository.save(MapFromDto(equityDTO)));
	}

	public Equity MapFromDto(EquityDTO equityDTO) {
		Equity equity = new Equity();
		equity.setSymbol(equityDTO.getSymbol());
		equity.setCurrency(equityDTO.getCurrency());
		equity.setName(equityDTO.getName());
		equity.setTimezone(equityDTO.getTimezone());
		equity.setRegion(equityDTO.getRegion());
		equity.setTimezone(equityDTO.getTimezone());
		equity.setType(equityDTO.getType());
		equity.setMarketClose(equityDTO.getMarketClose());
		equity.setMarketOpen(equityDTO.getMarketOpen());

		return equity;
	}
}
