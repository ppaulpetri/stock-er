package com.pmp.stock.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pmp.stock.repository.AlarmRepository;
import com.pmp.stock.repository.UserRepository;
import com.pmp.stock.repository.models.Alarm;
import com.pmp.stock.rest.errors.InternalServerErrorException;
import com.pmp.stock.security.SecurityUtils;
import com.pmp.stock.service.dto.AlarmDTO;

/**
 * Service class for managing alarms.
 */
@Service
@Transactional
public class AlarmService {

	private final AlarmRepository alarmRepository;
	private final UserRepository userRepository;
	private final EquityService equityService;

	public AlarmService(AlarmRepository alarmRepository, EquityService equityService, UserRepository userRepository) {
		this.alarmRepository = alarmRepository;
		this.equityService = equityService;
		this.userRepository = userRepository;
	}

	/*
	 * Creates a new alarm
	 */
	public Alarm createAlarm(AlarmDTO alarmDTO) {
		Alarm alarm = new Alarm();
		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			alarm.setActive(true);
			alarm.setMax(alarmDTO.getMax());
			alarm.setMin(alarmDTO.getMin());
			alarm.setValue(alarmDTO.getValue());
			alarm.setUser(user);
			alarm.setEquity(equityService.GetOrCreateEquity(alarmDTO.getEquity()));
			alarmRepository.save(alarm);
		});

		return alarm;
	}

	/*
	 * update alarm
	 */
	public AlarmDTO updateAlarm(AlarmDTO alarmDTO) {

		return Optional.of(alarmRepository.findById(alarmDTO.getId())).filter(Optional::isPresent).map(Optional::get)
				.map(alarm -> {
					alarm.setMax(alarmDTO.getMax());
					alarm.setMin(alarmDTO.getMin());
					alarm.setValue(alarmDTO.getValue());
					return alarm;
				}).map(AlarmDTO::new).orElseThrow(() -> new InternalServerErrorException("Alarm could not be updated"));
	}
	
	/*
	 * get alarm by id
	 */
	@Transactional(readOnly = true)
	public AlarmDTO getAlarm(long id) {
		return alarmRepository.findById(id).map(AlarmDTO::new).orElseThrow(() -> new InternalServerErrorException("Alarm could not be found"));		

	}

	/*
	 * Make alarm active/inactive
	 * 
	 * Only the user that created the alarm will be able to change it.
	 * #extraSecurity
	 */
	public void changeActiveStatus(boolean isActive, long id) {
		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			alarmRepository.findOneByIdAndUserId(id, user.getId()).ifPresent(alarm -> {
				alarm.setActive(isActive);
			});
		});
	}

	/*
	 * Delete alarm
	 * 
	 * Only the user that created the alarm will be able to delete it.
	 * #extraSecurity
	 */
	public void deleteAlarm(Long id) {
		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			alarmRepository.findOneByIdAndUserId(id, user.getId()).ifPresent(alarm -> {
				alarmRepository.delete(alarm);
			});
		});
	}

	/*
	 * Gets alarm for current logged in user
	 */
	public List<AlarmDTO> getAllAlrmsForUser() {
		List<AlarmDTO> alarms = new ArrayList<>();
		SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin).ifPresent(user -> {
			alarmRepository.findAllByUserId(user.getId()).stream().map(AlarmDTO::new).forEach(alarms::add);
		});
		return alarms;
	}
}
