package com.pmp.stock.service.dto;

import com.pmp.stock.repository.models.Equity;

public class EquityDTO {

	private Long id;

	private String symbol;

	private String name;

	private String type;

	private String region;

	private String marketOpen;

	private String marketClose;

	private String timezone;
	
	private String currency;
	
	public EquityDTO() {
		// Empty constructor needed for Jackson.
	}
	
	public EquityDTO(Equity equity) {
		this.id = equity.getId();
		this.type = equity.getType();
		this.name = equity.getName();
		this.symbol = equity.getSymbol();
		this.region = equity.getRegion();
		this.timezone = equity.getTimezone();
		this.currency = equity.getCurrency();
		this.marketOpen = equity.getMarketOpen();
		this.marketClose = equity.getMarketClose();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getMarketOpen() {
		return marketOpen;
	}

	public void setMarketOpen(String marketOpen) {
		this.marketOpen = marketOpen;
	}

	public String getMarketClose() {
		return marketClose;
	}

	public void setMarketClose(String marketClose) {
		this.marketClose = marketClose;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}
