package com.pmp.stock.service.tasks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.pmp.stock.properties.WatchDogConfiguration;
import com.pmp.stock.repository.AlarmRepository;
import com.pmp.stock.repository.EquityRepository;
import com.pmp.stock.repository.models.Alarm;
import com.pmp.stock.repository.models.Equity;
import com.pmp.stock.service.MailService;
import com.pmp.stock.service.enums.AlphaVantageSucks;

import static java.util.stream.Collectors.toSet;

/*
 * This class will be used for scheduling stock check
 * 
 * It has a fixed delay because we do not want to throttle the @App . It will wait fixed time between executions.
 */
@Component
public class StockWatchDog {

	private final Logger log = LoggerFactory.getLogger(StockWatchDog.class);

	private final MailService mailService;
	private final AlarmRepository alarmRepository;
	private final EquityRepository equityRepository;
	private final WatchDogConfiguration config;
	// this will store user's triggered alarms by user id so we can send a single
	// email for all alarms at once;
	private Map<Long, Set<Alarm>> userAlarmsPair;
	private List<Equity> equituies;
	private int failedIndex;
	private int retryNumber;

	private StockWatchDog(EquityRepository equityRepository, AlarmRepository alarmRepository, MailService mailService,
			WatchDogConfiguration config) {
		this.config = config;
		this.mailService = mailService;
		this.alarmRepository = alarmRepository;
		this.equityRepository = equityRepository;
		this.userAlarmsPair = new HashMap<>();
	}

	@Scheduled(initialDelay = 10000, fixedDelayString = "${watchdog.stockCheckIntervalInMiliseconds}")
	private void start() {
		log.info("Scheduler started for checking stocks.");

		failedIndex = 0;
		retryNumber = 0;

		equituies = equityRepository.findAll();
		checkEquityes(0);

		log.info("Scheduler finished checking stocks.");
	}

	private void checkEquityes(int idx) {
		failedIndex = idx;
		try {
			for (Equity eq : equituies.subList(idx, equituies.size())) {
				double value = getCurentEquityValue(eq.getSymbol());
				checkAlarms(eq, value);
				failedIndex++;
			}
			sendMailsForUsers();
		} catch (Exception e) {
			// 3 retries
			if (retryNumber < 4) {
				// API only allows 5 calls per minute so we'll do this ugly thing for now
				// normally we could use another token for API but that's another story
				try {
					Thread.sleep(60000);
					checkEquityes(failedIndex);
					retryNumber++;
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}

				log.debug("Filed equity at index " + failedIndex, e);
			} else {
				log.error("More than 3  failed attems while getting equityes ", e);
			}
		}
	}

	/*
	 * Get current equity price form www.alphavantage.co
	 */
	private double getCurentEquityValue(String symbol) throws Exception {
		Double currentValue = null;
		String uri = String.format(config.getEquityQuoteURI(), symbol, config.getApiKey());

		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
		try {
			JSONObject obj = new JSONObject(result);
			currentValue = obj.getJSONObject(AlphaVantageSucks.GLOBAL_QUOTE.getProperty())
					.getDouble(AlphaVantageSucks.PRICE.getProperty());
		} catch (JSONException e) {
			log.error("Error getting global quote for symbol " + symbol, e);
			log.error(result);
			//TODO create specific exception
			throw new Exception();
		}

		return currentValue;
	}

	/*
	 * If any equity price is not between min[ price ]max an email is sent to the
	 * user
	 */
	private void checkAlarms(Equity equity, Double price) {
		if (price == null) {
			return;
		}

		Set<Alarm> alarms = new HashSet<Alarm>();

		// I could not do it from query name because it has or and it needs parenthesis.
		// That's why are 2 queries.
		List<Alarm> maxChecked = alarmRepository.findAllByEquityIdAndActiveIsTrueAndMaxLessThan(equity.getId(), price);
		List<Alarm> minChecked = alarmRepository.findAllByEquityIdAndActiveIsTrueAndMinGreaterThan(equity.getId(),
				price);

		alarms.addAll(maxChecked);
		alarms.addAll(minChecked);

		assignAlarms(alarms);
		// mailService.sendAlarmMail(alarm);
	}

	/**
	 * @param @Set<Alarm> alarms triggered for a @Equity
	 * @return @Map<Long, Set<Alarm>> alarms grouped by user id
	 */
	private void assignAlarms(Set<Alarm> alarms) {

		// current alarms for equity
		Map<Long, Set<Alarm>> grouped = groupByUserId(alarms);

		// merge of maps and list values
		grouped.entrySet().stream()
				.forEach(entry -> userAlarmsPair.merge(entry.getKey(), entry.getValue(), (listTwo, listOne) -> {
					listOne.addAll(listTwo);
					return listOne;
				}));
	}

	/**
	 * @param @Set<Alarm> alarms triggered for a @Equity
	 * @return @Map<Long, Set<Alarm>> alarms grouped by user id
	 */
	private Map<Long, Set<Alarm>> groupByUserId(Set<Alarm> alarms) {
		return alarms.stream().collect(Collectors.groupingBy(alarm -> alarm.getUser().getId(), toSet()));
	}

	private void sendMailsForUsers() {
		userAlarmsPair.entrySet().stream().forEach(entry -> {

			StringBuilder messageContent = new StringBuilder("The following alarms : <br/> <br/>");
			entry.getValue().stream().forEach(alarm -> {
				messageContent.append(alarm.getEquity().getName()).append('[').append(alarm.getEquity().getSymbol())
						.append("]").append(" having price : ").append(alarm.getValue()).append(" ")
						.append(alarm.getEquity().getCurrency()).append("; <br/>");
			});
			messageContent.append("<br/><br/>are out of your alarm bounds.");

			// TODO getUser :)
			mailService.sendAlarmMail(entry.getValue().iterator().next().getUser(), messageContent.toString());
		});
	}
}
