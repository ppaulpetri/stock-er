package com.pmp.stock.service.enums;

/*
 * Not sure what's with those naming conventions
 * I hope hell's gates are open for him
 */
public enum AlphaVantageSucks {

	GLOBAL_QUOTE("Global Quote"), SYMBOL("01. symbol"), OPEN_VALUE("02. open"), HIGH_VALUE("03. high"), LOW_VALUE("04. low"), PRICE("05. price"),
	VOLUME("06. volume"), LAST_DAY("07. latest trading day"), LAST_CLOSE("08. previous close"), CHANGE("09. change"),
	CHANGE_PERCENT("10. change percent");

	private final String property;

	AlphaVantageSucks(String property) {
		this.property = property;
	}

	public String getProperty() {
		return property;
	}
}
