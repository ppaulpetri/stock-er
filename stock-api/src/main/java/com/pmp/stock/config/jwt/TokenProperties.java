package com.pmp.stock.config.jwt;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "token")
public class TokenProperties {
	@NotBlank
	private String secretKey;
	@Max(100)
	private int tokenValidityInDays;
	@Max(100)
	private int tokenValidityInDaysForRememberMe;

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public int getTokenValidityInDays() {
		return tokenValidityInDays;
	}

	public void setTokenValidityInDays(int tokenValidityInDays) {
		this.tokenValidityInDays = tokenValidityInDays;
	}

	public int getTokenValidityInDaysForRememberMe() {
		return tokenValidityInDaysForRememberMe;
	}

	public void setTokenValidityInDaysForRememberMe(int tokenValidityInDaysForRememberMe) {
		this.tokenValidityInDaysForRememberMe = tokenValidityInDaysForRememberMe;
	}
}
