import { Injectable } from "@angular/core";
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate
} from "@angular/router";
import { AccountService } from "../auth/account.service";

@Injectable({ providedIn: "root" })
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private accountService: AccountService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Promise<boolean> {
    const authorities = route.data["authorities"];
    // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
    // that the client has a principal too, if they already logged in by the server.
    // This could happen on a page refresh.
    return this.checkLogin(authorities, state.url);
  }

  checkLogin(authorities: string[], url: string): Promise<boolean> {
    return this.accountService.identity().then(
      account => {
        // no account for token
        if (!account) {
          this.router.navigate(["login"]).then(() => {});
        }

        if (!authorities || authorities.length === 0) {
          return true;
        }

        if (account) {
          const hasAnyAuthority = this.accountService.hasAnyAuthority(
            authorities
          );
          if (hasAnyAuthority) {
            return true;
          }

          return false;
        }

        this.router.navigate(["login"]).then(() => {});
        return false;
      },
      () => {
        console.log("to login on error");

        this.router.navigate(["login"]).then(() => {});
        return false;
      }
    );
  }
}
