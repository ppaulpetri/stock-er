import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { AlarmListComponent } from "./pages/alarm-list/alarm-list.component";
import { EquitySubscriberComponent } from "./pages/equity-subscriber/equity-subscriber.component";
import { AuthGuardService } from "../core/guards/auth-guard.service";

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: "list",
        component: AlarmListComponent
      },
      {
        path: "alarm",
        component: EquitySubscriberComponent
      },
      {
        path: "",
        redirectTo: "list",
        pathMatch: "full"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
