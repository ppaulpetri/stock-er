import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { ThemeModule } from '../@theme/theme.module';
import { RouterModule } from '@angular/router';
import { AlarmEditComponent } from './pages/alarm-edit/alarm-edit.component';
import { AlarmListComponent } from './pages/alarm-list/alarm-list.component';
import { EquitySubscriberComponent } from './pages/equity-subscriber/equity-subscriber.component';
import { PagesRoutingModule } from './pages-routing.module';
import { StatusCardComponent } from './pages/status-card/status-card.component';

@NgModule({
  imports: [PagesRoutingModule, CommonModule, FormsModule, CoreModule, ThemeModule, RouterModule],
  declarations: [
    PagesComponent,
    AlarmListComponent,
    AlarmEditComponent,
    EquitySubscriberComponent,
    StatusCardComponent
  ],
  exports: [AlarmListComponent, AlarmEditComponent, EquitySubscriberComponent, StatusCardComponent]
})
export class PagesModule {}
