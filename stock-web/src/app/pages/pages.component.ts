import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  menu: NbMenuItem[] = [
    {
      title: 'PAGE LEVEL MENU',
      group: true
    },
    {
      title: 'Your alarms',
      icon: 'nb-list',
      link: '/home/list'
    },
    {
      title: 'Create new',
      icon: 'nb-volume-high',
      link: '/home/alarm'
    }
  ];

  constructor() {}

  ngOnInit() {}
}
