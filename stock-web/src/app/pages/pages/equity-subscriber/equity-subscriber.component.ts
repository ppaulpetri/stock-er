import { Component, OnInit } from '@angular/core';
import { AlarmService } from '../../services/alarm.service';
import { Equity } from '../../models/equity';
import { NbSearchService } from '@nebular/theme';

@Component({
  selector: 'app-equity-subscriber',
  templateUrl: './equity-subscriber.component.html',
  styleUrls: ['./equity-subscriber.component.scss']
})
export class EquitySubscriberComponent {
  selectedEquity: Equity;
  equities: Array<Equity> = new Array();

  constructor(private alarmService: AlarmService, private searchService: NbSearchService) {
    this.searchService.onSearchSubmit().subscribe((search: any) => {
      this.startSearch(search.term);
    });
  }

  startSearch(keyWord: string) {
    this.equities = new Array();
    this.alarmService.searchStock(keyWord).subscribe(response => {
      if (response) {
        response.bestMatches.map(eq => this.equities.push(new Equity(eq)));
      }
    });
  }

  subscribeEquity(equity: Equity) {
    this.selectedEquity = equity;
  }

  onClose(_event: any) {
    this.selectedEquity = null;
  }
}
