import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";

@Component({
  selector: "ngx-status-card",
  styleUrls: ["./status-card.component.scss"],
  template: `
    <nb-card (click)="onStatusClick($event)" [ngClass]="{ off: !on }">
      <div class="icon-container">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>

      <div class="details">
        <div class="title">{{ title }}</div>
        <div class="status">{{ on ? "ON" : "OFF" }}</div>
      </div>
    </nb-card>
  `
})
export class StatusCardComponent implements OnInit {
  @Input() on = true;
  @Input() type: string;
  @Input() title: string;
  @Input() active: boolean;

  @Output() onStatusChanged: EventEmitter<boolean> = new EventEmitter();

  ngOnInit(): void {
    this.on = this.active;
  }

  onStatusClick(event) {
    this.on = !this.on;
    this.onStatusChanged.emit(this.on);
    event.stopPropagation();
  }
}
