import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AlarmService } from "../../services/alarm.service";
import { Equity } from "../../models/equity";
import { Alarm } from "../../models/alarm";
import { GlobalQuote } from "../../models/global-quote";
import { NbToastrService } from "@nebular/theme";

@Component({
  selector: "app-alarm-edit",
  templateUrl: "./alarm-edit.component.html",
  styleUrls: ["./alarm-edit.component.scss"]
})
export class AlarmEditComponent implements OnInit {
  @Input() alarm?: Alarm;
  @Input() equity: Equity;

  editMode: boolean;
  submitted: boolean;
  globalQuote: GlobalQuote;
  @Output() onClose: EventEmitter<Alarm> = new EventEmitter();

  constructor(
    private alarmService: AlarmService,
    private toastrService: NbToastrService
  ) {}

  get hasMinError() {
    return (
      this.submitted &&
      (!this.alarm.min ||
        this.alarm.min < 0 ||
        this.alarm.min >= this.alarm.value)
    );
  }

  get hasMaxError() {
    return (
      this.submitted && (!this.alarm.max || this.alarm.max <= this.alarm.value)
    );
  }

  ngOnInit() {
    if (!this.alarm) {
      this.alarm = new Alarm(this.equity);
    } else {
      this.editMode = true;
    }
    this.getEquityCurrentMarketValue();
  }

  getEquityCurrentMarketValue() {
    this.alarmService
      .getEquityValue(this.alarm.equity.currency)
      .subscribe((data: any) => {
        this.globalQuote = new GlobalQuote(data);
        this.alarm.value = this.globalQuote.price;
      });
  }

  onSave() {
    this.submitted = true;
    if (!this.hasMaxError && !this.hasMinError) {
      if (this.editMode) {
        this.alarmService.updateAlarm(this.alarm).subscribe(
          (response: Alarm) => {
            this.onClose.emit(response);
            this.toastrService.success("Alarm updated", "Success");
          },
          () => {
            this.toastrService.danger("Cannot update this alarm", "Error");
          }
        );
      } else {
        this.alarmService.createAlarm(this.alarm).subscribe(
          (response: Alarm) => {
            this.onClose.emit(response);
            this.toastrService.success("Alarm created", "Success");
          },
          () => {
            this.toastrService.danger("Cannot create this alarm", "Error");
          }
        );
      }
    }
  }

  onCancel() {
    this.onClose.emit(null);
  }
}
