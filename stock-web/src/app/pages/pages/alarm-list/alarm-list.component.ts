import { Component } from "@angular/core";
import { AlarmService } from "../../services/alarm.service";
import { Alarm } from "../../models/alarm";
import { NbToastrService } from "@nebular/theme";

@Component({
  selector: "app-alarm-list",
  templateUrl: "./alarm-list.component.html",
  styleUrls: ["./alarm-list.component.scss"]
})
export class AlarmListComponent {
  loading = true;
  alarmList: Array<Alarm> = [];
  editedAlarm: Alarm;

  constructor(
    private alarmService: AlarmService,
    private toastrService: NbToastrService
  ) {
    this.fetchAlarms();
  }

  fetchAlarms(): void {
    this.alarmService.getAllAlarms().subscribe(
      (response: Array<Alarm>) => {
        this.loading = false;
        this.alarmList = response;
      },
      () => {
        // TODO handle errors;
        this.loading = false;
      }
    );
  }

  editAlarm(alarm: Alarm) {
    this.alarmService
      .getAlarm(alarm.id)
      .subscribe(response => (this.editedAlarm = response));
  }

  deleteAlarm(alarm: Alarm) {
    this.alarmService.deleteAlarm(alarm.id).subscribe(() => {
      this.toastrService.success("Alarm deleted", "Success");
      this.fetchAlarms();
    });
  }

  onAlarmEdited(_event: any) {
    this.editedAlarm = null;
    this.fetchAlarms();
  }

  onStatusChanged(isActive: boolean, alarm: Alarm) {
    this.alarmService
      .changeStatus(alarm.id, isActive)
      .subscribe(() =>
        this.toastrService.success("Alarm status changed", "Success")
      );
  }
}
