import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Alarm } from '../models/alarm';
import { SERVER_API_URL } from 'src/app/app.constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlarmService {
  private apiKey = 'R14E72KHYDTWWALV';
  constructor(private http: HttpClient) {}

  searchStock(keyword: string) {
    return this.http.get<any>(
      `https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${keyword}&apikey=${this.apiKey}`
    );
  }

  getEquityValue(code: string) {
    return this.http.get<any>(
      `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${code}&apikey=${this.apiKey}`
    );
  }

  createAlarm(alarm: Alarm): Observable<any>  {
    return this.http.post<Alarm>(SERVER_API_URL + 'alarms', alarm, { observe: 'response' });
  }

  updateAlarm(alarm: Alarm): Observable<any>  {
    return this.http.put<Alarm>(SERVER_API_URL + 'alarms', alarm, { observe: 'response' });
  }

  changeStatus(id: number, isActive: boolean): Observable<any>  {
    return this.http.patch(SERVER_API_URL + `alarms/${id}/status/${isActive}`, isActive);
  }

  deleteAlarm(id: number): Observable<any>  {
    return this.http.delete(SERVER_API_URL + `alarms/${id}`);
  }

  getAllAlarms(): Observable<Array<Alarm>> {
    return this.http.get<Array<Alarm>>(SERVER_API_URL + 'alarms');
  }

  getAlarm(id: number): Observable<Alarm> {
    return this.http.get<Alarm>(SERVER_API_URL + `alarms/${id}`);
  }
}
