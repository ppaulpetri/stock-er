export class Equity {
  symbol: string;
  name: string;
  type: string;
  region: string;
  marketOpen: string;
  marketClose: string;
  timezone: string;
  currency: string;
  constructor(data: any) {
    if (data) {
      const keys = Object.keys(data);

      this.symbol = data[keys[0]];
      this.name = data[keys[1]];
      this.type = data[keys[2]];
      this.region = data[keys[3]];
      this.marketOpen = data[keys[4]];
      this.marketClose = data[keys[5]];
      this.timezone = data[keys[6]];
      this.currency = data[keys[7]];
    }
  }
}
