export class GlobalQuote {
  open: number;
  high: number;
  low: number;
  price: number;
  volume: number;
  constructor(data: any) {
    if (data) {
      data = data['Global Quote'];
      const keys = Object.keys(data);

      this.open = data[keys[1]];
      this.high = data[keys[2]];
      this.low = data[keys[3]];
      this.price = data[keys[4]];
      this.volume = data[keys[5]];
    }
  }
}

// 01. symbol: "USD"
// 02. open: "39.2700"
// 03. high: "39.2700"
// 04. low: "38.3167"
// 05. price: "38.6050"
// 06. volume: "20062"
// 07. latest trading day: "2019-03-06"
// 08. previous close: "39.4200"
// 09. change: "-0.8150"
// 10. change percent: "-2.0675%"
