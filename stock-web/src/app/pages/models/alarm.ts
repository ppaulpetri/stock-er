import { Equity } from './equity';

export class Alarm {
  id: number;
  min: number;
  max: number;
  value: number;
  equity: Equity;
  active: boolean;

  constructor(equity: Equity) {
    this.equity = equity;
  }
}
