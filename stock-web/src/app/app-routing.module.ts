import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared';
import { RegisterComponent } from './shared/register/register.component';
import { PasswordResetFinishComponent } from './shared/password-reset/finish/password-reset-finish.component';

const routes: Routes = [
  { path: 'home', loadChildren: './pages/pages.module#PagesModule' },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'reset',
    component: PasswordResetFinishComponent
  },
  {
    path: 'activate',
    component: PasswordResetFinishComponent
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
