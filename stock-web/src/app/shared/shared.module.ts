import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { NgbDateAdapter } from "@ng-bootstrap/ng-bootstrap";

import { NgbDateMomentAdapter } from "./util/datepicker-adapter";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { LoginComponent } from "./login/login.component";
import { HasAnyAuthorityDirective } from "./auth/has-any-authority.directive";
import { CoreModule } from "../core/core.module";
import { ThemeModule } from "../@theme/theme.module";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { RegisterComponent } from "./register/register.component";
import { PasswordResetFinishComponent } from "./password-reset/finish/password-reset-finish.component";

@NgModule({
  imports: [CommonModule, BrowserModule, FormsModule, CoreModule, ThemeModule, RouterModule ],
  declarations: [LoginComponent, PasswordResetFinishComponent, RegisterComponent, HasAnyAuthorityDirective],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
  entryComponents: [LoginComponent],
  exports: [LoginComponent, RegisterComponent, PasswordResetFinishComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule
    };
  }
}
