import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/core/auth/account.service';
import { ResetAccount } from './reset-account.model';

@Component({
  selector: 'password-reset-finish',
  styleUrls: ['./password-reset-finish.component.scss'],
  templateUrl: './password-reset-finish.component.html'
})
export class PasswordResetFinishComponent implements OnInit, AfterViewInit {
  key: string;
  error: string;
  success: string;
  submitted = false;
  resetAccount: ResetAccount;
  showMessages: any;
  messages: string[];
  doNotMatch: string;
  keyMissing: boolean;

  constructor(
    private renderer: Renderer,
    private route: ActivatedRoute,
    private elementRef: ElementRef,
    private accountService: AccountService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.key = params['key'];
    });
    this.resetAccount = new ResetAccount;
    this.keyMissing = !this.key;
  }

  ngAfterViewInit() {
    if (this.elementRef.nativeElement.querySelector('#password') != null) {
      this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
    }
  }

  finishReset() {
    this.doNotMatch = null;
    this.error = null;
    if (this.resetAccount.password !== this.resetAccount.confirmPassword) {
      this.doNotMatch = 'ERROR';
    } else {
      this.accountService.savePassword({ key: this.key, newPassword: this.resetAccount.password }).subscribe(
        () => {
          this.success = 'OK';
        },
        () => {
          this.success = null;
          this.error = 'ERROR';
        }
      );
    }
  }
}
