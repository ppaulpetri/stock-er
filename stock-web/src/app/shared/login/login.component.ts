import { Component, AfterViewInit, ElementRef, Renderer2, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/core/login/login.service';
import { User } from 'src/app/core/user/user.model';

@Component({
  selector: 'app-login-component',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html'
})
export class LoginComponent implements AfterViewInit {
  user: User;
  credentials: any;
  rememberMe: boolean;
  showMessages: any = {};
  authenticationError: boolean;
  errors: Array<any> = new Array();

  constructor(
    private router: Router,
    private renderer: Renderer,
    private elementRef: ElementRef,
    private loginService: LoginService
  ) {
    this.credentials = {};
    this.user = new User();
  }

  ngAfterViewInit() {
    setTimeout(
      () => this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#email'), 'focus', []),
      0
    );
  }

  cancel() {
    this.credentials = {
      username: null,
      password: null,
      rememberMe: true
    };
    this.authenticationError = false;
  }

  getConfigValue(key: any) {
    return '';
  }

  login() {
    this.loginService
      .login({
        username: this.user.email,
        password: this.user.password,
        rememberMe: this.rememberMe
      })
      .then(() => {
        this.authenticationError = false;
        this.router.navigate(['home']);
      })
      .catch(() => {
        this.authenticationError = true;
      });
  }

  register() {
    this.router.navigate(['/register']);
  }

  requestResetPassword() {
    this.router.navigate(['/reset', 'request']);
  }
}
