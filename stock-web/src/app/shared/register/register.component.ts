import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { AccountService } from "src/app/core/auth/account.service";
import {
  LOGIN_ALREADY_USED_TYPE,
  EMAIL_ALREADY_USED_TYPE
} from "../constants/error.constants";
import { User } from "src/app/core/user/user.model";
import { Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";

@Component({
  selector: "jhi-register",
  styleUrls: ["./register.component.scss"],
  templateUrl: "./register.component.html"
})
export class RegisterComponent implements OnInit {
  confirmPassword: string;
  doNotMatch: string;
  error: string;
  errorEmailExists: string;
  errorUserExists: string;
  registerAccount: any;
  success: boolean;
  modalRef: NgbModalRef;
  messages: string[];
  user: User = new User();

  constructor(
    private router: Router,
    private accountService: AccountService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit() {
    this.success = false;
  }

  register() {
    // TODO hard coded for now./ mechanism in place if internationalization needed
    this.user.langKey = "en";
    this.user.login = this.user.email;

    this.accountService.save(this.user).subscribe(
      () => {
        this.success = true;
        this.toastrService.success("Account created", "Success");
        this.router.navigate(["login"]).then(() => {});
      },
      response => this.processError(response)
    );
  }

  private processError(response: HttpErrorResponse) {
    this.success = null;
    this.toastrService.danger("Cannot create account", "Error");
    if (
      response.status === 400 &&
      response.error.type === LOGIN_ALREADY_USED_TYPE
    ) {
      this.errorUserExists = "ERROR";
    } else if (
      response.status === 400 &&
      response.error.type === EMAIL_ALREADY_USED_TYPE
    ) {
      this.errorEmailExists = "ERROR";
    } else {
      this.error = "ERROR";
    }
  }
}
